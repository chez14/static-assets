# chez14's static content repo

This repository are intended to give me a robust, flexible, free, high-uptime content hosting for things such as forum signature assets, myanimelist profile, and email signature.

This might also intended to hold things such as signed documents, signed statement or other things that doesn't need dynamic content server.

This repo is served under domain `https://gl-statis.konten.christianto.net` and proxied under Cloudflare network. All SSL certificates wll be governed by Cloudflare.

## Relevant Links

- [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/introduction.html)
- [I made a CDN Hosted site, for free](https://blog.christianto.net/2020/03/28/i-made-a-cdn-hosted-site-for-free/?utm_source=service&utm_medium=repo-readme&utm_campaign=show-info)


